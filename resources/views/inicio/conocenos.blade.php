@extends('inicio.template.home')
@section('title')
        Sincos
@endsection

 
   

@section('content')


   

      <!-- Call to Action Well -->
      <div class="card text-white bg-secondary my-4 text-center">
        <div class="card-body">
          <p class="text-white m-0">Estadisticas Relevantes de Las Novedades e Irregularidades presentadas en el Departamento</p>
        </div>
      </div>

      <!-- Content Row -->
      <div class="row">
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Novedades Reportadas</h2>
              <p class="card-text">Podras listar las novedades registradas en el Departamento, en sus respectivos Municipios y Corregimientos</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Leer M&aacute;s</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Mayor Registro</h2>
              <p class="card-text">La novedad mas reportada es xxxxxx en el municipio xxxxx</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">More Info</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Card Three</h2>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus.</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">More Info</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->

      </div>
      <!-- /.row -->

    </div>

@endsection


<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title', 'admin') |Inicio</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet"  href="{{asset('plugins/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.css')}}" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('css/small-business.css')}}" rel="stylesheet">

 <link rel="stylesheet" href="{{asset('css/bod.css')}}" rel="stylesheet">

  </head>

		
  <body >
  		
	
			@include('inicio.template.partials.nav')

		
		<div style="height: 65px;"></div>
	
		<div class="container" >
			@yield('content')
		</div>
		


		@include('inicio.template.partials.footer')
		<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
		<script src="{{asset('vendor/jquery/jquery.js')}}"></script>
    	<script src="{{asset('vendor/popper/popper.js')}}"></script>
    	<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    	<script src="{{asset('vendor/bootstrap/js/bootstrap.js')}}"></script>
      <script src="{{asset('js/bod.js')}}"></script>

		 
</body>

</html>


    <nav class="navbar navbar-expand-lg navbar-light    fixed-top" id="menuhe" >
      <div class="container">
      <a class="navbar-brand" href="#"> <img  src="{{asset('images/sincos.png')}}" id="logo" /></a>
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
       <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link"  href="{{asset('home')}}" >Inicio
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{asset('inicio/Conocenos')}}">Conocenos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{asset('inicio/User')}}">Guia de Usuario</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{asset('inicio/Contact')}}">Contactanos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{asset('inicio/Login')}}">Iniciar Sesi&oacute;n</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

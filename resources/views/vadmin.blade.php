
@extends('admin.template.admin')
@section('title')
        Sincos
@endsection

 
   

@section('content')


   

      <!-- Heading Row -->
      <div class="row my-4"  >
        <div class="col-lg-8">
          <img class="img-fluid rounded" src="http://placehold.it/900x400" alt="">
        </div>
        <!-- /.col-lg-8 -->
        <div class="col-lg-4">
          <h1>SINCOS</h1>
          <p align="justify">Bienvenidos al Sistema de Informaci&oacute;n de Novedades Y Control de Seguridad Y Convivencia -SINCOS, si has presenciado o tienes conocimiento de alguna irregularidad que afecte
          la seguridad y convivencia de t&uacute; Municipio, corregimiento o localidad, te invitamos a Reportar. </p>
          <a class="btn btn-primary btn-lg" href="#">Reporta Irregularidades</a>
        </div>
        <!-- /.col-md-4 -->
      </div>
      <!-- /.row -->

      <!-- Call to Action Well -->
      <div class="card text-white bg-secondary my-4 text-center">
        <div class="card-body">
          <p class="text-white m-0">Estadisticas Relevantes de Las Novedades e Irregularidades presentadas en el Departamento</p>
        </div>
      </div>

      <!-- Content Row -->
      <div class="row">
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Novedades Reportadas</h2>
              <p class="card-text">Podras listar las novedades registradas en el Departamento, en sus respectivos Municipios y Corregimientos</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Leer M&aacute;s</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Mayor Registro</h2>
              <p class="card-text">La novedad mas reportada es xxxxxx en el municipio xxxxx</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">More Info</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 mb-4">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Card Three</h2>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus.</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">More Info</a>
            </div>
          </div>
        </div>
        <!-- /.col-md-4 -->

      </div>
      <!-- /.row -->

    </div>

@endsection


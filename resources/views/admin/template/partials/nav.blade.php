

    <nav class="navbar navbar-expand-lg navbar-light    fixed-top" id="menuhe" >
      <div class="container">
      <a class="navbar-brand" href="#"> <img  src="{{asset('images/sincos.png')}}" id="logo"  /></a>
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
       <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
              <a class="nav-link" href="{{asset('paneladmin/Novedad')}}">Novedad</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{asset('paneladmin/Listado_Novedades')}}">Listar Novedades</a>
            </li>
            <li class="nav-item">
           
              <div class="dropdown nav-item">
                  <a class=" dropdown-toggle nav-link  " type="" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Localidades   <span class="caret"></span></a>
                  
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="background-color: #75B6FF;">
                    <li class="nav-item"><a class="nav-link" href="{{asset('paneladmin/Listado_Municipios')}}">Municipios</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{asset('paneladmin/Listado_Corregimientos')}}">Corregimientos</a></li>
                    
                  </ul>
                </div>
            </li>

            <li class="nav-item">
           
              <div class="dropdown nav-item">
                  <a class=" dropdown-toggle nav-link  " type="" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Reportes   <span class="caret"></span></a>
                  
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a class="nav-link" href="{{asset('paneladmin/reportes/Reprt_novedad')}}">Reporte por Novedad</a></li>
                    <li><a class="nav-link" href="{{asset('paneladmin/reportes/Reprt_novedad_userr')}}">Reporte por Novedad de Ciudadanos</a></li>
                    <li><a class="nav-link" href="{{asset('paneladmin/reportes/Comparativo_edades')}}">Comparativo de Edades</a></li>
                    <li><a class="nav-link" href="{{asset('paneladmin/reportes/Comparativo_municipios')}}">Comparativo de Municipios</a></li>
                    
                    
                  </ul>
                </div>
            </li>
              <li class="nav-item">
           
              <div class="dropdown nav-item">
                  <a class=" dropdown-toggle nav-link  " type="" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Admin   <span class="caret"></span></a>
                  
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a class="nav-link" href="{{asset('paneladmin/user/Config_Cuenta')}}">Cuenta</a></li>
                    <li><a class="nav-link" href="{{asset('paneladmin/user/Config_Sistema')}}">Confugurac&iacute;n del Sistema</a></li>
                    
                  </ul>
                </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{asset('home')}}">Salir</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
